package com.cdek.contractcache.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

/**
 * Базовый класс-сущность договор (фэйковый договор)
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BaseContract implements Serializable {

	/** id договора */
    private Long id;
    /** uuid контракта (требования по передаче сущностей по шине */
    private UUID uuid;
    /** Номер договора */
    private String number;
    
    /** Дата создания договора */
    private Date creationDate;
    /** Дата заключения договора */
    private Date startDate;
    /** Дата окончания договора */
    private Date endDate;

    private Boolean hasAdditionalDocuments;

    private Boolean isVIPClient;
    private Boolean createConsolidatedShipmentOrder;
    /**Флаг осуществления надбавки за НДС*/
    private Boolean removeVatAllowance;
    /** Надбавка за НДС */
    private Long vatAllowance;
    /** Есть ли персональный тариф */
    private Boolean hasPersonalTariff;

    /** Кредитный лимит */
    private Integer creditLimit;
    /** Отсрочка  оплаты (дни) */
    private Integer paymentDeferment;

    /** Признак того что контрагент платит наличкой*/
    private Boolean paysInCash;

    /**
     * Услуги для оформления возврата:
     * включить услугу "доставка до двери" - заменяет тарифы на "склад-дверь", кроме магистрального экспресса и экономичного экспресса
     */
    private Boolean deliverToDoor;

    /**
     * Добавить услугу до двери для тарифов, доставляющих только до склада - для магистрального экспресса и экономичного экспресса
     */
    private Boolean addDoorService;

    /** Флаг с фронта - есть ли реверс, дает понять нужно ли проверять его наличие или же стоит удалить */
    private Boolean hasReverseRule;

    /**
     * Нужно ли оповещать клиента при пересечении грузом границы с РФ
     */
    private Boolean notifyOnCrossingRussianBorder;
}
