package com.cdek.contractcache.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;

import java.time.Duration;

import static java.util.Collections.singletonMap;

/**
 * Кастомная конфигурация кеша Redis
 */
@Configuration
@EnableCaching
public class CacheConfig {

    public static final String CONTRACT_CACHE = "contract";
    public static final String REDIS_CACHE_MANAGER = "redisCacheManager";

    @Value("${cache_expiration_time_sec:18000}")
    private long cacheExpirationTimeSec;

    @Value("${redis.host}")
    private String host;

    @Value("${redis.port}")
    private int port;

    @Bean
    public RedisConnectionFactory redisConnectionFactory() {
        RedisStandaloneConfiguration config = new RedisStandaloneConfiguration(host, port);
        return new JedisConnectionFactory(config);
    }

    @Bean
    public RedisCacheConfiguration cacheConfiguration() {
        return RedisCacheConfiguration.defaultCacheConfig()
                .entryTtl(Duration.ofSeconds(cacheExpirationTimeSec))
                .prefixKeysWith(CONTRACT_CACHE + ":")
                .disableCachingNullValues();
    }

    @Bean(name = REDIS_CACHE_MANAGER)
    public RedisCacheManager cacheManager() {
        return RedisCacheManager.builder(redisConnectionFactory())
                .cacheDefaults(cacheConfiguration())
//                .withInitialCacheConfigurations(singletonMap("predefined", cacheConfiguration().disableCachingNullValues()))
                .transactionAware()
                .build();
    }
}
