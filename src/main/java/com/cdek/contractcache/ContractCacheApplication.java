package com.cdek.contractcache;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ContractCacheApplication {

    public static void main(String[] args) {
        SpringApplication.run(ContractCacheApplication.class, args);
    }
}
