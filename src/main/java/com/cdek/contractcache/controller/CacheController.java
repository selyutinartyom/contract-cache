package com.cdek.contractcache.controller;

import com.cdek.contractcache.entity.BaseContract;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import static com.cdek.contractcache.config.CacheConfig.CONTRACT_CACHE;
import static com.cdek.contractcache.config.CacheConfig.REDIS_CACHE_MANAGER;

/**
 * Контроллер для работы с кэшем Redis
 */
@Slf4j
@Controller
@RequestMapping(value = "cache")
@CacheConfig(cacheManager = REDIS_CACHE_MANAGER) // кастомный CacheManager
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class CacheController {

    /**
     * Добавляет фэйковый объект в кэш договора
     *
     * @param id идентификатор фэйкового договора
     * @return фэйковый договор
     */
    @GetMapping("add/{id}")
    @Cacheable(value = CONTRACT_CACHE, key = "#id")
    @ResponseBody
    public BaseContract addOne(@PathVariable Long id) {
        log.info("Request cache/add/{}", id);

        // Вместо заглушки
        BaseContract contract = BaseContract.builder()
                .id(id).number("test_" + id)
                .build();

        log.info("Response cache/add/{} with contract {}.", id, contract);
        return contract;
    }

    /**
     * Очищает весь кэш договора
     *
     * @return OK
     */
    @GetMapping("flush")
    @CacheEvict(value = CONTRACT_CACHE, allEntries = true)
    @ResponseBody
    public String flush() {
        log.info("Run cache/flush");
        return "OK";
    }

    /**
     * Очищает CONTRACT_CACHE по id договора
     *
     * @param id идентификатор договоаа
     * @return OK
     */
    @GetMapping("flush/{id}")
    @CacheEvict(value = CONTRACT_CACHE, key = "#id")
    @ResponseBody
    public String flushById(@PathVariable Long id) {
        log.info("Run cache/flush/{} ", id);
        return "OK";
    }
}
